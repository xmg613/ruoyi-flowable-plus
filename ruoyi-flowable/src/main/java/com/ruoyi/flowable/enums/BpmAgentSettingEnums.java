package com.ruoyi.flowable.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Bin
 * @version 1.0
 * @date 2023/9/7 15:37
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum BpmAgentSettingEnums {


    YOUR_SELF(1, "我的代理人"),
    OTHERS(2, "我代理的人"),
    ;

    private Integer status;
    private String describe;
}
