package com.ruoyi.flowable.core.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 流程代理设置
 * </p>
 *
 * @author caobin
 * @since 2023-09-07
 */
@Data
@Table("bpm_agent_setting")
public class BpmAgentSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @Id
    private Long id;

    /**
     * 标题
     */
    private String subject;

    /**
     * 授权人ID
     */
    private String authId;

    /**
     * 开始生效时间
     */
    private LocalDateTime startDate;

    /**
     * 失效时间
     */
    private LocalDateTime endDate;

    /**
     * 创建人ID
     */
    private String createBy;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 代理范围 1,全部流程,2,部分流程
     */
    private Integer agentType;

    /**
     * 更新人ID
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 委托说明
     */
    private String opinion;

    /**
     * 代理人id
     */
    private String assignee;
}
