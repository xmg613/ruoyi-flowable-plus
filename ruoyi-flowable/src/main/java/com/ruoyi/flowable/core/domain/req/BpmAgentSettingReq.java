package com.ruoyi.flowable.core.domain.req;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Bin
 * @version 1.0
 * @date 2023/9/7 14:30
 */
@Data
public class BpmAgentSettingReq {

    /**
     * 标题
     */
    private String subject;

    /**
     * 授权人ID
     */
    private String authId;

    /**
     * 开始生效时间
     */
    private LocalDateTime startDate;

    /**
     * 失效时间
     */
    private LocalDateTime endDate;


    /**
     * 代理范围 1,全部流程,2,部分流程
     */
    private Integer agentType;


    /**
     * 委托说明
     */
    private String opinion;

    /**
     * 代理人id
     */
    private String assignee;
}
