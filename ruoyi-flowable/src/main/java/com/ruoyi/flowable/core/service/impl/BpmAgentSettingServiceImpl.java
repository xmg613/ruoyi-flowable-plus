package com.ruoyi.flowable.core.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.update.UpdateWrapper;
import com.ruoyi.flowable.core.domain.BpmAgentSetting;
import com.ruoyi.flowable.core.domain.req.BpmAgentSettingReq;
import com.ruoyi.flowable.core.domain.resp.BpmAgentSettingResp;
import com.ruoyi.flowable.core.mapper.BpmAgentSettingMapper;
import com.ruoyi.flowable.core.service.BpmAgentSettingService;
import com.ruoyi.flowable.enums.BpmAgentSettingEnums;
import com.ruoyi.flowable.utils.TaskUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Bin
 * @version 1.0
 * @date 2023/9/7 14:23
 */
@Service
public class BpmAgentSettingServiceImpl implements BpmAgentSettingService {
    @Autowired
    BpmAgentSettingMapper settingMapper;

    @Override
    public List<String> getAssigneeByAuthId(String authId, LocalDate taskCreateTime) {
//        QueryWrapper query = QueryWrapper.create()
//            .select()
//            .from(BPM_AGENT_SETTING)
//            .where(BPM_AGENT_SETTING.AUTH_ID.eq(authId))
//            .and(BPM_AGENT_SETTING.START_DATE.le(taskCreateTime))
//            .and(BPM_AGENT_SETTING.END_DATE.ge(taskCreateTime));
//
//        List<BpmAgentSetting> bpmAgentSettings = settingMapper.selectListByQuery(query);
//        List<String> assignees = bpmAgentSettings.stream().map(BpmAgentSetting::getAssignee).collect(Collectors.toList());
//        return assignees;
        return null;
    }

    @Override
    public void addAssignee(BpmAgentSettingReq req) {
//        BpmAgentSetting bpmAgentSetting = new BpmAgentSetting();
//        BpmAgentSetting newAccount = UpdateWrapper.of(bpmAgentSetting)
//            .set(BPM_AGENT_SETTING.AGENT_TYPE,req.getAgentType())
//            .set(BPM_AGENT_SETTING.ASSIGNEE,req.getAssignee())
//            .set(BPM_AGENT_SETTING.END_DATE,req.getEndDate())
//            .set(BPM_AGENT_SETTING.START_DATE,req.getStartDate())
//            .set(BPM_AGENT_SETTING.AUTH_ID,req.getAuthId())
//            .set(BPM_AGENT_SETTING.OPINION,req.getOpinion())
//            .toEntity();
//        settingMapper.insert(newAccount);
    }

    @Override
    public List<BpmAgentSettingResp> getAgentSetting(String queryTyp) {
//        List<BpmAgentSettingResp> list = new ArrayList<>();
//        QueryWrapper query = QueryWrapper.create().from(BPM_AGENT_SETTING);
//
//        if (BpmAgentSettingEnums.YOUR_SELF.equals(queryTyp)){
//            query.select()
//                .where(BPM_AGENT_SETTING.AUTH_ID.eq(TaskUtils.getUserId()));
//            List<BpmAgentSetting> bpmAgentSettings = settingMapper.selectListByQuery(query);
//
//            for (BpmAgentSetting bpmAgentSetting : bpmAgentSettings) {
//                BpmAgentSettingResp respOne = new BpmAgentSettingResp();
//                BeanUtil.copyProperties(bpmAgentSetting,respOne);
//                respOne.setEmployeeNo(bpmAgentSetting.getAssignee());
//                list.add(respOne);
//            }
//        }else {
//            query.select()
//                .where(BPM_AGENT_SETTING.ASSIGNEE.eq(TaskUtils.getUserId()));
//            List<BpmAgentSetting> bpmAgentSettings = settingMapper.selectListByQuery(query);
//
//            for (BpmAgentSetting bpmAgentSetting : bpmAgentSettings) {
//                BpmAgentSettingResp respOne = new BpmAgentSettingResp();
//                BeanUtil.copyProperties(bpmAgentSetting,respOne);
//                respOne.setEmployeeNo(bpmAgentSetting.getAuthId());
//                list.add(respOne);
//            }
//        }
//        return list;
        return  null;
    }
}
