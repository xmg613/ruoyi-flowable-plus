package com.ruoyi.flowable.core.mapper;

import com.mybatisflex.core.BaseMapper;
import com.ruoyi.flowable.core.domain.BpmAgentSetting;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 流程代理设置 Mapper 接口
 * </p>
 *
 * @author caobin
 * @since 2023-09-07
 */
@Mapper
public interface BpmAgentSettingMapper extends BaseMapper<BpmAgentSetting> {

}
