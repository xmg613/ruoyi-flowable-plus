package com.ruoyi.flowable.core.service;


import com.ruoyi.flowable.core.domain.BpmAgentSetting;
import com.ruoyi.flowable.core.domain.req.BpmAgentSettingReq;
import com.ruoyi.flowable.core.domain.resp.BpmAgentSettingResp;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 流程代理设置 服务类
 * </p>
 *
 * @author caobin
 * @since 2023-09-07
 */
public interface BpmAgentSettingService {
    //获取当前任务的代理人
    List<String> getAssigneeByAuthId(String authId, LocalDate taskCreateTime);

    void  addAssignee(BpmAgentSettingReq req);

    List<BpmAgentSettingResp> getAgentSetting(String queryTyp);

}
