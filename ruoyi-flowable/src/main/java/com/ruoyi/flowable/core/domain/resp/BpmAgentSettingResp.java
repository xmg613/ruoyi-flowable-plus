package com.ruoyi.flowable.core.domain.resp;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author Bin
 * @version 1.0
 * @date 2023/9/7 14:44
 */
@Getter
@Setter
public class BpmAgentSettingResp {

    /**
     * 开始生效时间
     */
    private LocalDateTime startDate;

    /**
     * 失效时间
     */
    private LocalDateTime endDate;


    /**
     * 我代理的人或者是代理我的人的工号
     */
    private String employeeNo;
}
