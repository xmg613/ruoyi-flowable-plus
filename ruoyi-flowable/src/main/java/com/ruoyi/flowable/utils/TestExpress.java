package com.ruoyi.flowable.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TestExpress {

    public void test() {
        System.out.println("我通过表达式直接调用了，嘎嘎牛掰！！！");
    }

}
