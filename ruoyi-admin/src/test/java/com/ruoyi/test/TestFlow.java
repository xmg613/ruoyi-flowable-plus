package com.ruoyi.test;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.flowable.common.constant.ProcessConstants;
import com.ruoyi.flowable.utils.ModelUtils;
import com.ruoyi.workflow.service.IWfProcessService;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.Model;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.sql.SQLOutput;

/**
 * @author Bin
 * @version 1.0
 * @date 2023/8/10 9:49
 */
@SpringBootTest
public class TestFlow {
    @Autowired
    RepositoryService repositoryService;
    @Test
    public void test01(){
        // 获取流程模型
        Model model = repositoryService.getModel("47c03bac-3717-11ee-a2e0-a0e70b8d65c1");
        if (ObjectUtil.isNull(model)) {
            throw new RuntimeException("流程模型不存在！");
        }
        // 获取流程图
        byte[] bpmnBytes = repositoryService.getModelEditorSource("47c03bac-3717-11ee-a2e0-a0e70b8d65c1");
        if (ArrayUtil.isEmpty(bpmnBytes)) {
            throw new RuntimeException("请先设计流程图！");
        }

        String bpmnXml = StringUtils.toEncodedString(bpmnBytes, StandardCharsets.UTF_8);
        BpmnModel bpmnModel = ModelUtils.getBpmnModel(bpmnXml);
        String processName = model.getName() + ProcessConstants.SUFFIX;
        System.out.println(processName);
    }

    @Resource
    IWfProcessService processService;
    @Test
    public void test02(){
        processService.queryProcessDetail("171abc6f-eec5-11ee-a9b2-a0e70b8d65c1", "171d066f-eec5-11ee-a9b2-a0e70b8d65c1");
    }


}
