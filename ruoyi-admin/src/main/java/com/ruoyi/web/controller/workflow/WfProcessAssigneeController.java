package com.ruoyi.web.controller.workflow;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.flowable.core.domain.req.BpmAgentSettingReq;
import com.ruoyi.flowable.core.domain.resp.BpmAgentSettingResp;
import com.ruoyi.flowable.core.service.BpmAgentSettingService;
import com.ruoyi.workflow.domain.bo.WfModelBo;
import com.ruoyi.workflow.domain.vo.WfModelVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Bin
 * @version 1.0
 * @date 2023/9/7 15:58
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/workflow/assignee")
public class WfProcessAssigneeController {

    @Autowired
    BpmAgentSettingService bpmAgentSettingService;


    @SaCheckPermission("workflow:assignee:add")
    @GetMapping("/add")
    public R<String> list(@RequestBody BpmAgentSettingReq req) {
       bpmAgentSettingService.addAssignee(req);
       return R.ok("成功");
    }

    @SaCheckPermission("workflow:assignee:getType")
    @GetMapping("/getType")
    public R<List<BpmAgentSettingResp>> list(String type) {
        List<BpmAgentSettingResp> agentSetting = bpmAgentSettingService.getAgentSetting(type);
        return R.ok(agentSetting);
    }
}
